Folder for the Express middlewares.

- Extracts common controller code, which should be executed on multiple requests.
- Never accesses the db directly, that's for models
