Folder contains all the routes the app will serve.
Controllers handle web requests, serves the templates to the user.

- Controllers never access db directly, that's for models
- Also contains index.js file: load all other controllers
    -> router holds all routes, only one that application loads at startup
