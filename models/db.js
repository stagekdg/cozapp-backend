var mongoose = require('mongoose');
var gracefulShutdown;
var dbConnection = 'mongodb://admin:admin@localhost:27017/CozAppDB';
mongoose.connect(dbConnection);

//connection events
mongoose.connection.on('connected', function(){
	console.log('Mongoose connected to ' + dbConnection);
});

mongoose.connection.on('error', function(){
	console.log('Mongoose connection error ' + dbConnection);
});

mongoose.connection.on('disconnected', function(){
	console.log('Mongoose disconnected');
});

/*
gracefulShutdown = function(msg, callback){
	mongoose.connection.close(function () {
		console.log('Mongoose disconnected through ' + msg);
		callback();
	});
}
*/
